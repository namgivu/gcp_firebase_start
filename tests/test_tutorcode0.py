#region load DB_URL from .env
import os
from dotenv import load_dotenv

CH = os.path.abspath(os.path.dirname(__file__))
AH = f'{CH}/..'
load_dotenv(f'{AH}/.env')
DB_URL = os.environ.get('DB_URL') ; assert DB_URL, f'Envvar DB_URL is required, plese clone {AH}/.env-tempalte to .env and define one'
#endregion load DB_URL from .env


from firebase import firebase
firebase = firebase.FirebaseApplication(DB_URL, authentication=None)

'''
ref. morioh https://morioh.com/p/4dca3ded4cea
'''
class Test:

    def test0_post(self):
        '''
        googlesearch > python with firebase ~tutorial ~better
        '''

        '''
        more on installing package python-firebase as above ref not clear+detail enough
        
        googlesearch > python firebase ~guide
            > https://firebase.google.com/docs/database/rest/start
            this will lead us to python helper lib 
                > ref. https://github.com/ozgur/python-firebase
                
                origincommand to install package
                > pipenv install requests python-firebase
                > .              1        2
                
                with pipenv 
                00 Pipfile
                01 pipenv --rm ; pipenv install ; pipenv run pip install  git+https://github.com/ozgur/python-firebase
                #  .             1                2=workaround to install latest python-firebase ref. https://stackoverflow.com/a/63286825/248616  ggsearch. from .async import process_pool SyntaxError: invalid syntax
        
                with pipenv all-in-Pipefile way 
                > put this package line to Pipefile
                > python-firebase = {git = "ssh://git@github.com/ozgur/python-firebase.git"}
        '''

        d =  {
            's': 'abb ccc',
            'i': 122,
            'f': 1.22,
        }

        r = firebase.post('/somedir/subdir/', data=d)  # returned nodename eg {'name': '-MmqBHrZsGNM8gTk26oQ'}
        assert isinstance(r, dict)
        assert 'name' in r


    #region firebase get
    def test1_get(self):
        r_all = firebase.get('/somedir/subdir/', name='')  # returned list of node as a dict eg {'-MmqBHrZsGNM8gTk26oQ': {'f': 1.22, 'i': 122, 's': 'abb ccc'},
                                                           #                                     '-MmqQFwbRYJwlnfb2Er6': {'f': 1.22, 'i': 122, 's': 'abb ccc'}, ... }
        assert isinstance(r_all, dict)

        for name, d in r_all.items():
            assert isinstance(name, str)
            assert isinstance(d,    dict)


    def test1b_get(self):
        nodepath = '/somedir1b/subdir1b/'
        INP_d    = {'abb':122}  # aka input_dict

        r = firebase.post(nodepath, data=INP_d)
        added_name = r['name']

        r_atname = firebase.get(nodepath, name=added_name)
        assert isinstance(r_atname, dict)
        assert r_atname.get('abb') == 122 == INP_d.get('abb')
        #      1=actual               2=expected
    #endregion firebase get


    def test2_put(self):
        p = '/somedir2/subdir2/'  # aka nodepath

        #region prepare test's fixture
        d = {'abb':122}  # aka FIXTURE_nodedata
        r = firebase.post(p, data=d)
        n = r['name']  # aka FIXTURE_nodename
        #endregion

        # try @ add new key
        firebase.put(f'{p}/{n}', 'any key to update', 'some value')

        #region try @ update the current
        d_beforeput = firebase.get(p, n) ; assert d_beforeput['abb'] == 122
        firebase.put(f'{p}/{n}', 'abb', 333)

        d_afterput = firebase.get(p, n)

        assert d_afterput['abb'] == 333     !=      122 == d_beforeput['abb']
        #      1=node p/n after upd at abb  differ  2=node p/n before upd at abb
        #endregion try @ update the current


    def test3_delete(self):
        p = '/somedir3/subdir3/'  # aka nodepath

        #region prepare test's fixture
        d = {'abb':122}  # aka FIXTURE_nodedata
        r = firebase.post(p, data=d)
        n = r['name']  # aka FIXTURE_nodename
        #endregion

        # delete w/ any invalid path won't raise error
        firebase.delete(p, f'{n}/anysubpath')

        d_beforedel = firebase.get(p, n) ; assert d_beforedel is not None
        firebase.delete(p, n)
        d_afterdel = firebase.get(p, n) ; assert d_afterdel is None


    def test4_deleteall_outputofabovetest(self):

        def deleteall(p:'node path'):
            d_before = firebase.get(p, name='')
            if d_before:
                assert len(d_before.keys()) > 0

                firebase.delete(p, name='')

                d_after = firebase.get(p, name='')
                assert d_after is None

        deleteall('/somedir/subdir/')
        deleteall('/somedir1b/subdir1b/')
        deleteall('/somedir2/subdir2/')


#TODO test w/ protected firebasedb, currently it is test-mode db ie publicly accessible
